/*
********************************************
  PAPELERA INTELIGENTE
********************************************
*/
#define TRIGGER D1
#define ECHO    D2

// Placa Pin D1 > TRIGGER | Pin D2 > ECHO

#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// Auth lo da la app.
char auth[] = "9ae53912bb084a71b96660b0d977df4f";

// Datos del WiFi.
// Para redes sin contraseña dejar contrasena = "".
char nombre_wifi[] = "sinergia-interno";
char contrasena[] = "SeanTanInnovadoresComoValientes";
boolean lleno = false;
void setup() {

  Serial.begin (115200);
  Blynk.begin(auth, nombre_wifi, contrasena);
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
}
void loop() {
  while (!lleno) {
    long duracion, distancia;
    digitalWrite(TRIGGER, LOW);
    delayMicroseconds(2);

    digitalWrite(TRIGGER, HIGH);
    delayMicroseconds(10);

    digitalWrite(TRIGGER, LOW);
    duracion = pulseIn(ECHO, HIGH);
    distancia = (duracion / 2) / 29.1;
    if (distancia < 20) {
      delay(2000);
      digitalWrite(TRIGGER, LOW);
      delayMicroseconds(2);
      digitalWrite(TRIGGER, HIGH);
      delayMicroseconds(10);
      digitalWrite(TRIGGER, LOW);
      duracion = pulseIn(ECHO, HIGH);
      distancia = (duracion / 2) / 29.1;
      if (distancia < 20) {
        Blynk.notify("Se llenó la basura!");
        lleno = true;
      }
    }
    Serial.print(distancia);
    Serial.println("Centimeter:");
    Blynk.virtualWrite(V5, distancia);
    delay(200);
    Blynk.run();
  }
}
