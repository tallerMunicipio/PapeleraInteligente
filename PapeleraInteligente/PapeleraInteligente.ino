#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <Blynk.h>
/*
********************************************
  PAPELERA INTELIGENTE
********************************************
*/
#define TRIGGER 1
#define ECHO    2

// Placa Pin D1 > TRIGGER | Pin D2 > ECHO

#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space


// Auth lo da la app.
char auth[] = "<codigo de la aplicacion>";

// Datos del WiFi.
// Para redes sin contraseña dejar contrasena = "".
char nombre_wifi[] = "";
char contrasena[] = "";
boolean lleno = false;
void setup() {

  Serial.begin (9600);
  Blynk.begin(auth, nombre_wifi, contrasena);
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
}
void loop() {
  while (!lleno) {
    long duracion, distancia;
    digitalWrite(TRIGGER, LOW);
    delayMicroseconds(2);

    digitalWrite(TRIGGER, HIGH);
    delayMicroseconds(10);

    digitalWrite(TRIGGER, LOW);
    duracion = pulseIn(ECHO, HIGH);
    distancia = (duracion / 2) / 29.1;
    if (distancia < 20) {
      delay(2000);
      digitalWrite(TRIGGER, LOW);
      delayMicroseconds(2);
      digitalWrite(TRIGGER, HIGH);
      delayMicroseconds(10);
      digitalWrite(TRIGGER, LOW);
      duracion = pulseIn(ECHO, HIGH);
      distancia = (duracion / 2) / 29.1;
      if (distancia < 20) {
        Blynk.notify("Se llenó la basura!");
        lleno = true;
      }
    }
    Serial.print(distancia);
    Serial.println("Centimeter:");
    Blynk.virtualWrite(V5, distancia);
    delay(200);
    Blynk.run();
  }
}
